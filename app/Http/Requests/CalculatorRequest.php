<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CalculatorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(): array
    {
        // TODO @laravel-test
        return [
            'a' => 'required|integer',
            'b' => 'required|integer',
        ];
    }

    public function getA(): int
    {
        // TODO @laravel-test
        return Request::query('a');
    }

    public function getB(): int
    {
        // TODO @laravel-test
        return Request::query('b');
    }


}
