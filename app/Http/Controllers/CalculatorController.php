<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calculator as Cal;
use App\Http\Requests\CalculatorRequest;
use Validator;

class CalculatorController extends Controller
{

    public function add(CalculatorRequest $request)
    {
        // TODO @laravel-test
        // $request->validate();
        $request->validate($request->rules());
        $calculator = new Cal();
        $answer = $calculator->add($request->getA(), $request->getB());

        $response = [
            'result'=> $answer,
        ];
        return response()->json($response);
    }





    public function sub(CalculatorRequest $request)
    {
        // TODO @laravel-test
        $request->validate($request->rules());
        $calculator = new Cal();
        $answer = $calculator->subtract($request->getA(), $request->getB());
        
        $response = [
            'result'=> $answer,
        ];
        return response()->json($response);
    }



    public function div(CalculatorRequest $request)
    {
        // TODO @laravel-test
        $request->validate($request->rules());
        if ($request->getB() <= 0) { abort(403); }
        $calculator = new Cal();
        $answer = $calculator->divide($request->getA(), $request->getB());
        
        $response = [
            'result'=> $answer,
        ];
        return response()->json($response);
    }



    public function mul(CalculatorRequest $request)
    {
        // TODO @laravel-test
        $calculator = new Cal();
        $answer = $calculator->multiply($request->getA(), $request->getB());
        
        $response = [
            'result'=> $answer,
        ];
        return response()->json($response);
    }



    public function mod(CalculatorRequest $request)
    {
        // TODO @laravel-test
        $request->validate($request->rules());
        if ($request->getB() <= 0) { abort(403); }
        $calculator = new Cal();
        $answer = $calculator->modulo($request->getA(), $request->getB());
        
        $response = [
            'result'=> $answer,
        ];
        return response()->json($response);
    }
}
