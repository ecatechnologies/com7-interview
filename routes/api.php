<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// TODO @laravel-test
Route::group(['prefix'=>'calculator','middleware'=>'auth:api'], function () {

		Route::get('add', 'CalculatorController@add');
		Route::get('sub', 'CalculatorController@sub');
		Route::get('div', 'CalculatorController@div');
		Route::get('mul', 'CalculatorController@mul');

		Route::group(['middleware'=>'is_premium:api'], function () {
			Route::get('mod', 'CalculatorController@mod');
		});

});